# vmoptions-configuration
A very good configuration of vmoptions file for best performance  of IntelliJ products (IntelliJ WebStorm PyCharm etc..)

Please go to the path of the App installation (the default will be: C:\Program Files\JetBrains\[PyCharm,IntelliJ,WebStorm...]\bin

1. Replace the content in the webstorm64.exe.vmpoptions file with the one in the repository.

2. Replace or Create a new file into the path above and paste the idea.properties file into that path.

And Enjoy a much BETTER performance of your IDE.

Or Hasson
